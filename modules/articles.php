<?php
$templating->load('articles');

if (!isset($_GET['view']))
{
	// better than showing a blank page
	$templating->set_previous('title', 'Nothing to see here', 1);
	$core->message("There must have been an error as accessing this page directly doesn't do anything, be sure to report exactly what you did.", NULL, 1);
}

$types_allowed = ['any', 'all'];
// from clicking a single category tag, it wouldn't be set yet
if (!isset($_GET['type']))
{
	$type = 'any';
}
else if (in_array($_GET['type'], $types_allowed))
{
	$type = $_GET['type'];
}
else
{
	$type = 'any';
}

// for pagination, how many per page and actual page number
$articles_per_page = 15;
if (core::is_number($_SESSION['articles-per-page']))
{
	$articles_per_page = $_SESSION['articles-per-page'];
}

$page = core::give_page();

$allowed_views = ['none', 'cat', 'multiple'];
$view = 'none';
if (isset($_GET['view']) && in_array($_GET['view'], $allowed_views))
{
	$view = $_GET['view'];
}

if (isset($view))
{
	if ($view == 'none')
	{
		$_SESSION['message'] = 'none_found';
		$_SESSION['message_extra'] = 'categories';
		header("Location: /index.php?module=search");
		die();
	}
	
	// viewing a single category
	if ($view == 'cat')
	{
		if (!isset($_GET['catid']) || (isset($_GET['catid']) && empty($_GET['catid'])))
		{
			$_SESSION['message'] = 'none_found';
			$_SESSION['message_extra'] = 'categories';
			header("Location: /index.php?module=search");
			die();
		}

		$safe_category = core::make_safe($_GET['catid']);
		$safe_category = str_replace('-', ' ', $safe_category);
		$db->sqlquery("SELECT `category_id`, `category_name` FROM `articles_categorys` WHERE `category_name` = ?", array($safe_category));
		if ($db->num_rows() == 0)
		{
			$_SESSION['message'] = 'none_found';
			$_SESSION['message_extra'] = 'categories';
			header("Location: /index.php?module=search");
			die();
		}
		$get_category = $db->fetch();

		$templating->set_previous('meta_description', 'GamingOnLinux viewing Linux gaming news from the '.$get_category['category_name'].' category', 1);
		$templating->set_previous('title', 'Article category: ' . $get_category['category_name'], 1);

		$templating->block('category');
		$templating->set('category', $get_category['category_name']);
		
		article::display_category_picker();
		
		$safe_ids[] = $get_category['category_id'];
		
		$all_sql = '';
		if ($type == 'all')
		{
			$safe_ids[] = $get_category['category_id'];
			$all_sql = 'having count(r.`category_id`) = ?';
		}

		$db->sqlquery("SELECT r.`article_id` FROM `article_category_reference` r JOIN `articles` a ON a.`article_id` = r.`article_id` WHERE r.category_id IN (?) GROUP BY r.`article_id` $all_sql", $safe_ids);
		$total_items = $db->num_rows();

		if ($total_items > 0)
		{
			$paging_url = "/index.php?module=articles&view=cat&catid=" . $get_category['category_name'] . '&amp;';
			
			// sort out the pagination link
			$pagination = $core->pagination_link($articles_per_page, $total_items, $paging_url, $page);
			
			$cat_sql = 'r.`category_id` IN (?)';
		}
	}
	
	// viewing multiple categories
	else if ($view == 'multiple')
	{
		$templating->set_previous('meta_description', 'GamingOnLinux viewing Linux gaming news from mutiple categories', 1);
		$templating->set_previous('title', 'Searching for articles in multiple categories', 1);

		$templating->block('multi', 'articles');

		if (isset($_GET['catid']) || !isset($_GET['catid']))
		{
			if (!is_array($_GET['catid']))
			{
				$_SESSION['message'] = 'none_found';
				$_SESSION['message_extra'] = 'categories';
				header("Location: /index.php?module=search");
				die();
			}
			$categorys_ids = $_GET['catid'];
		}
		
		article::display_category_picker($categorys_ids);
		
		// sanitize, force to int as that's what we require
		foreach ($categorys_ids as $k => $make_safe)
		{
			$safe_ids[$k] = (int) $make_safe;
		}
		
		// this is really ugly, but I can't think of a better way to do it
		$count_array = count($safe_ids);
		$counter = 1;
		
		$cat_sql = '';
		foreach ($safe_ids as $cat)
		{
			$cat_sql .= '?';
			if ($counter < $count_array)
			{
				$cat_sql .= ',';
			}
			$counter++;
		}
		
		$cat_sql = sprintf(" r.`category_id` IN (%s) ", $cat_sql);
		
		// count how many there is in total
		$all_sql = '';
		if ($type == 'all')
		{
			$all_sql = 'having count(r.`category_id`) = ' . $count_array;
		}
		
		// otherwise, pick articles that have any of the selected tags
		$db->sqlquery("SELECT r.`article_id` FROM `article_category_reference` r JOIN `articles` a ON a.`article_id` = r.`article_id` WHERE $cat_sql GROUP BY r.`article_id` $all_sql", $safe_ids);
		
		$total_items = $db->num_rows();
		
		if ($total_items > 0)
		{
			$for_url = '';
			foreach ($safe_ids as $cat_url_id)
			{
				$safe_url_id = core::make_safe($cat_url_id);
				$for_url .= 'catid[]=' . $safe_url_id . '&amp;';
			}

			$paging_url = "/index.php?module=articles&view=multiple&" . $for_url;
			
			// sort out the pagination link
			$pagination = $core->pagination_link($articles_per_page, $total_items, $paging_url, $page);
		}
	}
	
	if (isset($total_items) && $total_items > 0)
	{
		$db->sqlquery("SELECT
			r.`article_id`,
			a.`author_id`,
			a.`title`,
			a.`slug`,
			a.`tagline`,
			a.`text`,
			a.`date`,
			a.`comment_count`,
			a.`guest_username`,
			a.`tagline_image`,
			a.`show_in_menu`,
			a.`gallery_tagline`,
			t.`filename` as gallery_tagline_filename,
			u.`username`
			FROM `article_category_reference` r
			JOIN `articles` a ON a.`article_id` = r.`article_id`
			LEFT JOIN ".$core->db_tables['users']." u on a.`author_id` = u.`user_id`
			LEFT JOIN `articles_tagline_gallery` t ON t.`id` = a.`gallery_tagline`
			WHERE $cat_sql AND a.`active` = 1
			GROUP BY r.`article_id` $all_sql
			ORDER BY a.`date` DESC LIMIT {$core->start}, $articles_per_page", $safe_ids);
		$articles_get = $db->fetch_all_rows();

		$count_rows = $db->num_rows();

		if ($count_rows > 0)
		{
			$article_id_array = array();

			foreach ($articles_get as $article)
			{
				$article_id_array[] = $article['article_id'];
			}
			$article_id_sql = implode(', ', $article_id_array);

			// this is required to properly count up the rank for the tags
			$db->sqlquery("SET @rank=null, @val=null");

			$category_tag_sql = "SELECT * FROM (
				SELECT r.article_id, c.`category_name` , c.`category_id`,
				@rank := IF( @val = r.article_id, @rank +1, 1 ) AS rank, @val := r.article_id
				FROM  `article_category_reference` r
				INNER JOIN  `articles_categorys` c ON c.category_id = r.category_id
				WHERE r.article_id
				IN ( $article_id_sql )
				ORDER BY CASE WHEN ($cat_sql) THEN 0 ELSE 1 END, r.`article_id` ASC
				) AS a
				WHERE rank < 5";
			$db->sqlquery($category_tag_sql, $safe_ids);
			$get_categories = $db->fetch_all_rows();

			$article_class->display_article_list($articles_get, $get_categories);
				
			$templating->block('bottom');
			$templating->set('pagination', $pagination);
		}
	}
	else
	{
		$core->message('No articles found with those options.');
	}
}
