<?php
return [
	"none_found" => [
		"text" => "We couldn't find the people requested! Have you got the correct exact username? Please try again.",
		"error" => 1
	]
];
