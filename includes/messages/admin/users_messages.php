<?php
return [
	"user_banned" =>
	[
		"text" => "That user has now been banned!",
	],
	"user_unbanned" => 
	[
		"text" => "That users ban has now been lifted!",
	],
	"user_content_deleted" =>
	[
		"text" => 'All the content from that user has now been deleted!'
	],
	"ip_unban" =>
	[
		"text" => 'That IP ban has now been lifted!'
	],
	"ip_ban" =>
	[
		"text" => 'That IP has now been banned!'
	]
];
?>
