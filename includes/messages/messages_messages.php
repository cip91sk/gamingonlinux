<?php
return [
	"notfound" => 
	[
		"text" => "We couldn't find the people requested! Have you got the correct exact username? Please try again.",
		"error" => 1
	],
	"pm_sent" =>
	[
		"text" => "That PM has now been sent!"
	]
];
