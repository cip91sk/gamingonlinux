<?php
return [
	"username_taken" => 
	[
		"text" => "Sorry but that username is taken, please try another! If you have forgotten your password, <a href=\"https://www.gamingonlinux.com/index.php?module=login&forgot\">click here to start a lost password request.</a>",
		"error" => 1
	],
	"username_characters" =>
	[
		"text" => 'Your username is not properly formatted. We only allow numbers, letters, "-" and "_".',
		"error" => 1
	],
	"email_taken" =>
	[
		"text" => 'Sorry but that email is taken, please try another! If you have forgotten your password, <a href="https://www.gamingonlinux.com/index.php?module=login&forgot">click here to start a lost password request.</a>',
		"error" => 1
	],
	"password_match" =>
	[
		"text" => "The password did not match the password confirmation, please try again!",
		"error" => 1
	]
];
?>
