<?php
return [
	"too_big" => 
	[
		"text" => "The file size was too big!",
		"error" => 1
	],
	"not_image" =>
	[
		"text" => "That was not an image!",
		"error" => 1
	],
	"cant_upload" =>
	[
		"text" => "We couldn't upload the file!",
		"error" => 1
	],
	"no_file" =>
	[
		"text" => "No file selected to upload, dummy!",
		"error" => 1
	]
];
