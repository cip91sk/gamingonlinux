<?php
return [
	"locked" => [
		"text" => "Sorry, this topic is currently locked so you cannot reply!",
		"error" => 1
	]
];
